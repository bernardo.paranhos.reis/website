# Who are we

We are a vACC part of VATEUD and VATSIM. With members from several countries and all kinds of ages, the passion for aviation and the flight simulation hobby is what drives our community.


# What we do

Portugal vACC provides high quality ATC and pilot training within Portuguese airspace on the VATSIM network. We train our controllers and pilots with the highest standards to be a part of the virtual skies, while having fun, learning and developing friendships.


# Staff

## ACCPT1 - Director
**810882 - Daniel Gomes** <daniel.gomes@portugal-vacc.org>

## ACCPT2 - Training Director
**1107190 - Miguel Frias** <miguel.frias@portugal-vacc.org>

## ACCPT3 - LPPC FIR Director
**1096507 - Bernardo Reis** <bernardo.reis@portugal-vacc.org>

## ACCPT4 - Events and Public Relations Director
**1433524 - Rodrigo Araújo** <rodrigo.araujo@portugal-vacc.org>

## ACCPT5 - LPPO FIR Director
**1214252 - Hugo Santos** <hugo.santos@portugal-vacc.org>

## ACCPT7 - Webmaster
**1273225 - Tiago Vicente** <tiago.vicente@portugal-vacc.org>

## ACCPT8 - Webmaster Advisor
**1377186 - Pedro Rodrigues**

## ACCPT9 - Training Advisor
**1369259 - Alex Metcalfe** <training@portugal-vacc.org>

## ACCPT10 - LPPO FIR Advisor
**884178 - Marco Gonçalves**
