// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function get_color_labels(){
  return [
    "red",
    "green",
    "blue",
    "black",
    "silver",
    "magenta",
    "lightblue",
    "orange",
    "purple",
    "#806000"
  ]
}

function get_commun_aircrafts_labels(most_common_aircraft){
  const color_labels = this.get_color_labels()
  const labels = Array()
  const commun_aircrafts = JSON.parse(most_common_aircraft)
  commun_aircrafts.forEach(commun => {
    var span = document.createElement('span');
    span.classList.add('mr-2');

    var item = document.createElement('I');
    item.classList.add('fa');
    item.classList.add('fa-circle');
    item.style.color = color_labels[commun_aircrafts.indexOf(commun)];

    var label = document.createTextNode(' ' + commun.planned_aircraft);
    span.appendChild(item)
    span.appendChild(label)
    labels.push(commun.planned_aircraft)
    var el = document.getElementById('commun_aircrafts_legend');
    el.appendChild(span);
  });
  return labels
}

function get_commun_airlines_labels(most_common_airline){
  const color_labels = this.get_color_labels()
  const labels = Array()
  const commun_airlines = JSON.parse(most_common_airline)
  commun_airlines.forEach(commun => {
    var span = document.createElement('span');
    span.classList.add('mr-2');

    var item = document.createElement('I');
    item.classList.add('fa');
    item.classList.add('fa-circle');
    item.style.color = color_labels[commun_airlines.indexOf(commun)];

    var label = document.createTextNode(' ' + commun.airline__name);
    span.appendChild(item)
    span.appendChild(label)
    labels.push(commun.airline__name)
    var el = document.getElementById('commun_airlines_legend');
    el.appendChild(span);
  });
  return labels
}

function get_commun_aircrafts_data(most_common_aircraft){
  const data = Array()
  const commun_aircrafts = JSON.parse(most_common_aircraft)
  commun_aircrafts.forEach(commun => {
    data.push(commun.dcount)
  });
  return data
}

function get_commun_airlines_data(most_common_airline){
  const data = Array()
  const commun_airlines = JSON.parse(most_common_airline)
  commun_airlines.forEach(commun => {
    data.push(commun.dcount)
  });
  return data
}

function most_common_aircrafts(most_common_aircraft){
  var ctx = document.getElementById("commun_aircrafts_graph");

  var mygraph1 = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels:this.get_commun_aircrafts_labels(most_common_aircraft),
      datasets: [{
        data: this.get_commun_aircrafts_data(most_common_aircraft),
        backgroundColor: this.get_color_labels(),
        hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      legend: {
        display: false
      },
      cutoutPercentage: 80,
    },
  });
}

function most_common_airlines(most_common_airline){
  var ctx = document.getElementById("commun_airlines_graph");

  var mygraph1 = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels:this.get_commun_airlines_labels(most_common_airline),
      datasets: [{
        data: this.get_commun_airlines_data(most_common_airline),
        backgroundColor: this.get_color_labels(),
        hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      maintainAspectRatio: false,
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      legend: {
        display: false
      },
      cutoutPercentage: 80,
    },
  });
}
