from dataclasses import dataclass
from functools import singledispatchmethod
from uuid import UUID

from src.kernel.aggregate_root import AggregateRoot
from src.kernel.messages import MessageBuilder, command, event
from src.canonical_model.vatsim_ratings import PosicaoAtc

LPPC = UUID('ae736f03-e13d-42aa-a77e-af348f63efee')
LPPO = UUID('b7f25ebe-5975-4007-a7e4-f04a135bc3f2')

@command
class CriarSector:
    sector_id: UUID
    nome: str
    callsign: str
    posicao: str
    frequencia: float

@command
class AlterarFrequenciaSector:
    sector_id: UUID
    frequencia: float

@event
class SectorCriado:
    sector_id: UUID
    nome: str
    callsign: str
    posicao: str
    frequencia: float

@event
class FrequenciaSectorAlterada:
    sector_id: UUID
    frequencia: float

@dataclass
class Riv(AggregateRoot):
    def __post_init__(self):
        super().__post_init__()

        self._sectors = dict()

    @singledispatchmethod
    def handle(self, cmd): pass

    @singledispatchmethod
    def apply(self, cmd): pass

    @handle.register
    def _(self, cmd: CriarSector):
        sector_existe = cmd.sector_id in self._sectors.keys()
        if sector_existe or self._freq_atribuida(cmd.frequencia):
            return

        PosicaoAtc[cmd.posicao]

        event = self._get_event_builder(SectorCriado) \
            .caused_by(cmd) \
            .args(
                cmd.sector_id,
                cmd.nome,
                cmd.callsign,
                cmd.posicao,
                cmd.frequencia,
            ) \
            .build()
        self.apply_change(event)

    @handle.register
    def _(self, cmd: AlterarFrequenciaSector):
        if self._freq_atribuida(cmd.frequencia):
            return

        event = self._get_event_builder(FrequenciaSectorAlterada) \
            .caused_by(cmd) \
            .args(cmd.sector_id, cmd.frequencia) \
            .build()
        self.apply_change(event)

    @apply.register
    def _(self, evt: SectorCriado):
        self._sectors[evt.sector_id] = evt.frequencia

    @apply.register
    def _(self, evt: FrequenciaSectorAlterada):
        self._sectors[evt.sector_id] = evt.frequencia

    def _freq_atribuida(self, frequencia):
        return frequencia in self._sectors.values()

    def _get_event_builder(self, type):
        return MessageBuilder().aggregate(self.id).type(type)
