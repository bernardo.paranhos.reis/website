# Santa Maria Oceanic Frequencies:

- LPPO_FSS (Primary HF) - 132.070
- LPPO_1_FSS (Secondary HF) - 124.850
- LPPO_1_DEL (Oceanic Clearance) - 127.900
- LPPO_CTR (Radar) - 132.150


# Documentation

<a type="button"
    href="https://drive.google.com/file/d/1Cjcyz8AJQPfmJk-JpV8QsrOU5W7vUfsU/view?usp=sharing"
    name="button"
    class="btn">LPPO FIR Manual</a>


<a type="button"
    href="https://drive.google.com/file/d/147vq2hKJ27DMnUJMj5ONsMpe1Etq153L/view?usp=sharing"
    name="button"
    class="btn">LPPD Controller Briefing</a>


<a type="button"
    href="https://drive.google.com/file/d/1lwWoFJ7gMV4sFqVcCJYCsZ0EFHEAoFaA/view?usp=sharing"
    name="button"
    class="btn">LPLA Controller Briefing</a>
