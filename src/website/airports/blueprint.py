from flask import Blueprint, render_template, current_app as app

from . import entities
from . import data

bp = Blueprint('airports', __name__, template_folder='templates', url_prefix='/airports')

@bp.route("/")
def home():
    return render_template("airports/list.html")

@bp.route("/<icao>")
def view(icao):
    icao = icao.upper()
    info = entities.get_airport_informations(icao)
    frequencies = data.get_atc_positions_from_icao(app.db, icao)
    return render_template(
        "airports/view.html",
        icao=icao,
        info=info,
        frequencies=frequencies,
        AVWX_TOKEN=app.config['AVWX_TOKEN'],
        CHARTFOX_TOKEN=app.config['CHARTFOX_TOKEN'])
