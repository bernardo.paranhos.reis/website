def get_atc_positions_from_icao(db, icao):
    atc_positions = db['atcpositions'].find()
    return [x for x in atc_positions if icao in x['position']]
