from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, SelectField, SubmitField

class EditGroupsForm(FlaskForm):
    _id    = HiddenField('_id')
    groups = StringField('Groups')
    save  = SubmitField('Save')

    def set_user(self, user):

        self._id.data = user['_id']
        self.groups.data = ', '.join(user['groups'])

    def get_groups(self):
        return list(set(map(lambda x: x.strip().lower(), self.groups.data.split(','))))
