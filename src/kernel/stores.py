import pickle
import pymongo
import os
from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
from uuid import UUID

from .buses import MemoryBus

class AggregateVersionMismatch(RuntimeError): pass

@dataclass(frozen=True)
class EventDescriptor:
    aggregate_id: UUID
    aggregate_version: int
    timestamp: datetime
    data: 'Event'

class MemoryEventStore:
    def __init__(self, bus):
        self._bus = bus
        self._event_descriptors = dict()

    def save_events(self, aggregate_id: UUID, events, expected_version: int):
        version = expected_version

        # try get event descriptors, otherwise create empty list
        stored = self._event_descriptors.get(aggregate_id, [])
        if len(stored) == 0:
            self._event_descriptors[id] = stored

        # ensure current version is as expected
        elif stored[-1].version != version and version != -1:
            raise AggregateVersionMismatch()

        # iterate through events, increasing the version with each one processed
        for event in events:
            version += 1
            event.__dict__['aggregate_version'] = version
            now = datetime.utcnow()
            stored.append(EventDescriptor(aggregate_id, version, now, event))
            self._bus.publish(event)

    def get_events(self, id: UUID):
        return tuple(map(lambda e: e.data, self._event_descriptors.get(id, [])))

class FileEventStore:
    def __init__(self, bus, root_path):
        self._bus = bus
        self._path = root_path
        self._ns = FileNamingStrategy(root_path)

    def save_events(self, aggregate_id: UUID, events, expected_version: int):
        version = expected_version

        # try get event descriptors, otherwise create empty list
        stored = self._retrieve(aggregate_id)

        # ensure current version is as expected
        curr_version = -1 if len(stored) == 0 else stored[-1].aggregate_version
        if version != curr_version:
            err = f'Expected version {version} got {curr_version} for {aggregate_id}'
            raise AggregateVersionMismatch(err)

        # iterate through events, increasing the version with each one processed
        for event in events:
            version += 1
            event.__dict__['aggregate_version'] = version
            now = datetime.utcnow()
            self._store(EventDescriptor(aggregate_id, version, now, event))
            self._bus.publish(event)

    def get_events(self, aggregate_id: UUID):
        return tuple(map(lambda e: e.data, self._retrieve(aggregate_id)))

    def replay(self, bus):
        for path, subdirs, files in os.walk(self._path):
            for dir in subdirs:
                for event in self.get_events(dir):
                    bus.publish(event)

    def _store(self, descriptor):
        d = descriptor
        file = self._ns.get_file(d.aggregate_id, d.aggregate_version)
        with open(file, 'wb') as f:
            pickle.dump(d, f)

    def _retrieve(self, aggregate_id):
        descriptors = []
        for file in self._ns.get_files(aggregate_id):
            with open(file, 'rb') as f:
                descriptors.append(pickle.load(f))
        return descriptors

    def _get_aggregate_file(self, id):
        dir = str(id)[:6]
        os.makedirs(os.path.join(self._path, dir), exist_ok=True)
        return os.path.join(self._path, dir, str(id))

class FileNamingStrategy:
    dir_chunk_size = 2

    def __init__(self, root_path):
        self.root_path = root_path

    def get_file(self, aggregate_id, aggregate_version):
        dir = self._get_dir(aggregate_id)
        file = str(UUID(int=aggregate_version))
        return os.path.join(dir, file)

    def get_files(self, aggregate_id):
        dir = self._get_dir(aggregate_id)
        abs_path = lambda p: os.path.join(dir, p)
        return sorted(map(abs_path, os.listdir(self._get_dir(aggregate_id))))

    def _get_dir(self, aggregate_id):
        _id = str(aggregate_id)
        chunk = _id[:FileNamingStrategy.dir_chunk_size]
        result = os.path.join(self.root_path, chunk, _id)
        os.makedirs(result, exist_ok=True)
        return result

class MongoEventStore(FileEventStore):
    def __init__(self, bus, mongo_uri):
        super().__init__(bus, '')

        mongo = pymongo.MongoClient(mongo_uri)
        try:
            self._db = mongo.get_database()
        except pymongo.errors.ConfigurationError:
            self._db = mongo['event_store']

        self._db.events.create_index('aggregate_id')
        self._db.events.create_index('aggregate_version')
        self._db.events.create_index('timestamp')

    def replay(self, bus):
        order = (('timestamp', 1),)
        docs = self._db.events.find({}).sort(order)
        for d in tuple(map(self._to_descriptor, docs)):
            bus.publish(d.data)

    def _store(self, d):
        doc = {
            'aggregate_id': d.aggregate_id,
            'aggregate_version': d.aggregate_version,
            'timestamp': d.timestamp,
            'data': pickle.dumps(d.data),
        }
        self._db.events.insert(doc)

    def _retrieve(self, aggregate_id):
        lookup = {'aggregate_id': aggregate_id}
        order = (('aggregate_version', 1),)
        result = self._db.events.find(lookup).sort(order)
        return tuple(map(self._to_descriptor, result))

    def _to_descriptor(self, doc):
        return EventDescriptor(
            doc['aggregate_id'],
            doc['aggregate_version'],
            doc['timestamp'],
            renamed_loads(doc['data']),
        )

# loads files pickled with modules since renamed
# see: https://stackoverflow.com/a/53327348/3343753
import io
class RenameUnpickler(pickle.Unpickler):
    renamed_modules = {
        'airspace_sectors_service.rivs_aggregate.domain': 'src.airspace_sectors_service.rivs_aggregate.domain',
        'eventos_service.evento_aggregate.domain': 'src.eventos_service.evento_aggregate.domain',
    }

    def find_class(self, module, name):
        renamed_module = self.renamed_modules.get(module, module)
        return super(RenameUnpickler, self).find_class(renamed_module, name)

def renamed_load(file_obj):
    return RenameUnpickler(file_obj).load()

def renamed_loads(pickled_bytes):
    file_obj = io.BytesIO(pickled_bytes)
    return renamed_load(file_obj)
