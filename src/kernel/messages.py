from abc import ABC
from dataclasses import dataclass
from functools import singledispatchmethod
from uuid import UUID, uuid4

def message(cls):
    @dataclass(frozen=True)
    class Class(dataclass(cls, frozen=True), Message): pass
    Class.__name__ = cls.__name__
    Class.__qualname__ = cls.__qualname__
    Class.__module__ = cls.__module__
    return Class

command = message
event = message

@dataclass(frozen=True)
class Message(ABC):
    message_id: UUID
    correlation_id: UUID
    causation_id: UUID
    aggregate_id: UUID
    aggregate_version: int

class MessageBuilder:
    def __init__(self, id: UUID=None):
        if id is None:
            id = uuid4()
        self.message_id = id
        self.correlation_id = id
        self.causation_id = id
        self.aggregate_version = -1
        self._args = []
        self._kwargs = {}

    def caused_by(self, msg):
        self.correlation_id = msg.correlation_id
        self.causation_id = msg.message_id
        self.aggregate_id = msg.aggregate_id
        return self

    def aggregate(self, id: UUID, version: int=-1):
        self.aggregate_id = id
        self.aggregate_version = version
        return self

    def type(self, type: Message):
        self.message_type = type
        return self

    def args(self, *args):
        self._args = args
        return self

    def kwargs(self, **kwargs):
        self._kwargs = kwargs
        return self

    def build(self):
        return self.message_type(
            self.message_id,
            self.correlation_id,
            self.causation_id,
            self.aggregate_id,
            self.aggregate_version,
            *self._args,
            **self._kwargs,
        )
