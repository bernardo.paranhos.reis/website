from datetime import datetime

def create_atcsession(user_cid, session_type, position,
    startsdate, endsdate, startstime, endstime, mentor):
    starts = _get_datetime(startsdate, startstime)
    ends = _get_datetime(endsdate, endstime)
    return {
        'controller'  :   user_cid,
        'type'        :   session_type,
        'position'    :   position,
        'starts'      :   starts,
        'ends'        :   ends,
        'duration_min':   int((ends - starts).total_seconds() / 60),
        'mentor'      :   mentor
    }

def create_atcposition(position, identifier, frequency, min_rating):
    return {
        'position'  :   f'{position}_{identifier}',
        'frequency' :   float(frequency),
        'min_rating':   int(min_rating),
    }

def create_certification(user_cid, certification, assigned_by):
    return {
        'cid'           : user_cid,
        'certification' : certification,
        'assigned_date' : datetime.utcnow(),
        'assigned_for'  : assigned_by
    }

def get_user_position_by_type(_type, live_position, training_position):
    if _type == 'Live':
        return live_position
    return training_position

def _get_datetime(date, time):
    _date_splited = date.split('/')
    _time_splietd = time.split(':')
    return datetime(
        int(_date_splited[2]),
        int(_date_splited[1]),
        int(_date_splited[0]),
        int(_time_splietd[0]),
        int(_time_splietd[1])
    )

def _get_callendar_background_color_from_type(session_type):
    if session_type == 'Live':
        return '#4d94ff'
    elif session_type == 'SweatBox':
        return '#e68a00'
    return '#006600'

def to_calendar_format(session):
    back_color = _get_callendar_background_color_from_type(session['type'])
    return {
        'title'          : session['position'],
        'type'           : session['type'],
        'controller'     : session['controller'],
        'controller_name': session['controller_name'],
        'duration_min'   : session['duration_min'],
        'start'          : session['starts'].isoformat(),
        'end'            : session['ends'].isoformat(),
        'mentor'         : session['mentor'],
        'mentor_name'    : session['mentor_name'],
        'textColor'      :'#ffffff',
        'backgroundColor':back_color
    }

def add_certification(user, certification):
    user['atc_certifications'].append(certification)

def remove_certification(user, icao):
    _cert = _find_certification_from_icao(user['atc_certifications'], icao)
    user['atc_certifications'].remove(_cert)

def _find_certification_from_icao(certifications, icao):
    return next(cert for cert in certifications if cert['certification']==icao)
