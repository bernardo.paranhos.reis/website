import jwt
import json
import requests
from os import environ
from flask import render_template, request, url_for, redirect
from flask_login import login_required, login_user, current_user
from datetime import datetime, timedelta
from ts3.query import TS3QueryError
from uuid import UUID
from pymongo import MongoClient

from . import auth
from . import app

from .controllers import data as controllers_data
from .events.blueprint import read as read_events
from .pedro_home_page_read_model import PedroHomePageFacade
from .home_page_read_model import HomePageFacade, HomePageModel

db = MongoClient(environ.get('MONGO_URI2'))

HomePageModel.register(app.bus, db.home_page)

def _get_discord_registration_token():
    if not current_user.is_anonymous:
        user = auth.data.get_user_for_cid(app.db, current_user.id)
        discord_payl = {
            'exp': datetime.utcnow() + timedelta(hours=1),
            'nickname': f'{user["name"]} - {user["cid"]}',
            'atc_rating': user['atc_rating'],
            'pilot_ratings': user['pilot_ratings'],
        }
        return jwt.encode(discord_payl, app.config['DISCORD_SECRET']).decode()

@app.context_processor
def main_args():
    return {
        'discord_reg_token': _get_discord_registration_token(),
    }

@app.route('/')
def home():
    read_facade = HomePageFacade(db)
    view = read_facade.get_home_page_view()
    return render_template('public/home-page.html', **view)

@app.route('/pedro')
def pedro_home_page():
    read_facade = PedroHomePageFacade(app.db)
    view = read_facade.get_pedro_home_page_view()
    return render_template('public/pedro-home-page.html', **view)

@app.route('/tsviewer')
def tsviewer():
    return render_template('public/tsviewer-page.html')

@app.route("/onlinemap")
def onlinemap():
    now = datetime.utcnow().timestamp()
    return render_template("public/onlinemap-page.html", now=now)

@app.route("/error")
def error():
    title = request.args.get('title')
    error = request.args.get('error')
    return render_template("public/error-page.html", error=error, title=title)

@app.route("/dashboard")
@login_required
def dashboard():
    return render_template("dashboard/home-page.html")

@app.route("/profile")
@login_required
def profile():
    return render_template("dashboard/profile-page.html",
        atcsessions=list(controllers_data.get_atcsessions_from_user(app.db, current_user.cid)))

@app.route("/members")
@login_required
def members():
    users = list(auth.data.get_users(app.db))
    stats = auth.data.get_controller_stats(app.db)

    return render_template("dashboard/members-page.html", users=users, stats=stats)

@app.errorhandler(401)
def handle_401(error):
    return render_template('public/error-page.html', title='Unauthorized', error=error)

@app.errorhandler(500)
def handle_500(error):
    error = 'We changed a ton of stuff, logout and log back in to see if it fixes it.'

    return render_template('public/error-page.html', title='Ops', error=error)

@app.route('/about-us')
@app.route('/controller')
@app.route('/lppc/controller-procedures')
@app.route('/lppc/pilot-procedures')
@app.route('/lppc/sectors')
@app.route('/lppo/controller-procedures')
@app.route('/lppo/pilot-procedures')
def markdown_page():
    root = 'website/website/static/markdown'
    path = root + request.path + '.md'
    with open(path, 'rt') as f:
        content = f.read()
    view_model = {'title': '', 'content': content}
    return render_template('public/markdown-page.html', **view_model)
