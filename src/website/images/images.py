from gridfs import GridFS
from flask import current_app as app
from flask_pymongo import PyMongo
from bson.objectid import ObjectId

_db = GridFS(PyMongo(app).db, collection='images')

def save_image(image):
    return _db.put(image.getvalue())

def get(iid):
    return _db.get(ObjectId(iid))

