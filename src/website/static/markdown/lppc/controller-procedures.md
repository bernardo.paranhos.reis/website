# Documentation

<a type="button"
    href="https://drive.google.com/file/d/1ZKhrNtksHvsTDew7CFnE7z_PKvXGKK4t/view?usp=sharing"
    name="button"
    class="btn">LPPT SOP</a>


<a type="button"
    href="https://drive.google.com/file/d/1L60y5GwSJtwoLBTrA_5iH-sx06GE0H0i/view?usp=sharing"
    name="button"
    class="btn">LPMA SOP</a>


<a type="button"
    href="https://drive.google.com/file/d/1TgEK2r7iY6jfVM2Rs1fXWF0Anwb2Ogl4/view?usp=sharing"
    name="button"
    class="btn">LPFR SOP</a>

<a type="button"
    href="https://drive.google.com/file/d/1610J0qIK9aMgtFcw-z3AiMbaNUR56GMx/view?usp=sharing"
    name="button"
    class="btn">LPPR SOP</a>

<a type="button"
    href="https://vats.im/LoA_LPPC_LECM"
    name="button"
    class="btn">LoA Spain and Portugal</a>
