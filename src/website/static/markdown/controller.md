# Get started as a Virtual Controller!

Hi! First of all you must have a vatsim account, if you do not get started at [here](https://www.vatsim.net/join?)

You must be associated to Portugal vACC, if you are not do so by sending an email to members@vateud.net
saying "Please associate my VATSIM account to Portugal vACC to get started as an ATC!"

Once that is completed send an email to training@portugal-vacc.org requesting the theoretical exam for the S1 (Ground/Delivery) rating.

You have 30 days to perform the exam after starting it! If sucessfully completed email again training@portugal-vacc.org to get started in training! You will be placed in waiting list for an available mentor

You can find Vatsim´s Code of Conduct below:
[Code of Conduct](https://www.vatsim.net/documents/code-of-conduct)
