FROM python:3.8-slim

RUN apt-get update \
    && pip install --upgrade pip \
    && pip install gunicorn


RUN mkdir /app
WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY src/ /app/src/
COPY wsgi.py /app/wsgi.py
COPY config.py /app/config.py

COPY tests/ /app/tests/
RUN python -m unittest discover tests/

COPY server/ /app/server/

EXPOSE 80/tcp
ENV APP_CONFIG=production
CMD ["gunicorn", "--bind=0.0.0.0:80", "--log-file=-", "wsgi:app"]
