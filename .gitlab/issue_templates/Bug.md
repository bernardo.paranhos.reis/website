# Title
An ideal title is clear, short, and gives the developer a summary of what the bug is. It should contain the category of the bug or the component of the app where the bug occurred (e.g. Cart, UI, etc.) and the action or circumstances it occurred under. A clear title makes the report easy to find for the developer in order to identify duplicate reports and makes triaging bugs a whole lot easier.

E.g. [Profile] Profile picture blacked out when inside a chat.



# Description
This is an overview of the bug and how and when it happened, written in shorthand. This part should include more details than the title, like how frequently the bug appears if it is an intermittent error and the circumstances that seem to trigger it.



# Reproduction Steps
This should include the minimum steps needed to reproduce the bug. Ideally, the steps should be short, simple, and can be followed by anybody. With that being said, encourage your testers and users to submit too many details rather than too little. The goal is to allow the developer to reproduce the bug on their side to get a better idea of what might be going wrong. A bug report without repro steps is minimally useful and only serves to waste time and effort that can be dedicated to resolving more detailed reports; be sure to communicate that to your testers and in a way that makes sense to your end users.

## Example

 1. Launch App > Messages > New Message.
 1. Enter recipient and message but leave “Subject” blank.
 1. Tap Attach > Image and choose ABC.jpeg (attached to report)
 1. Tap Send



# Actual Result
This is the result or output that the tester or user observed.

## Example

Error displayed: “Invalid format”



# Expected Result
This is the result or output that was expected or intended.

## Example

.jpeg format supported and message is sent with empty “No Subject”



# Attachments
Attachments can be very helpful for the developer to pinpoint the issue quicker; a screenshot of the issue does a lot of explaining, especially when the issue is visual. Other extremely useful attachments like logs can, at the very least, point the developer in the right direction.



# Bug Reporting Do’s
 * DO read your report when you’re done. Make sure it is clear, concise and easy to follow.
 * DO be as specific as possible and don’t leave room for ambiguity.
 * DO reproduce the bug a few times and try to eliminate any unnecessary steps.
 * If you have found a workaround or additional steps that make the bug behave differently, DO include that in your report.
 * DO check if the bug has already been submitted. If it has, add your details to the bug in a comment.
 * DO respond to developer requests for additional information.



# Bug Reporting Don’ts
 * DON’T include more than one bug in your report. Tracking the progress and dependencies of individual bugs becomes an issue when there are several bugs in the report.
 * DON’T be critical or blaming. Bugs are inevitable and not always easy to fix.
 * DON’T speculate on what’s causing the bug. This can send the developer on a wild goose chase, so stick to the facts.
 * DON’T post anything that is not a bug. Developers love to hear your feedback, but posting it in the wrong channel will only clutter their workflow and cause unnecessary delays.



_[original source](https://instabug.com/blog/how-to-write-a-bug-report-the-ideal-bug-report/#:~:text=A%20good%20bug%20report%20should,essentially%20stumbling%20in%20the%20dark.)_
