import tempfile
import shutil
from datetime import datetime, timedelta
from unittest import TestCase
from uuid import uuid4

from src.kernel.buses import TestBus
from src.kernel.stores import FileEventStore
from src.kernel.messages import MessageBuilder
from src.eventos_service import (
    EventoCommandHandlers,
    CriarEvento,
    DefinirParametroEvento,
    PublicarEvento,
    EventoCriado,
    ParametroEventoDefinido,
    EventoPublicado,
)

def criar_evento_cmd(criado_por, inicio, horas):
    return MessageBuilder() \
        .type(CriarEvento) \
        .aggregate(uuid4(), -1) \
        .args(criado_por, inicio, inicio + timedelta(hours=horas))

class TestCriarEvento(TestCase):
    def setUp(self):
        self._tmp_dir = tempfile.mkdtemp()
        bus = TestBus()
        store = FileEventStore(bus, self._tmp_dir)
        EventoCommandHandlers.register(bus, store)
        self.bus = bus

    def tearDown(self):
        shutil.rmtree(self._tmp_dir)

    def test_whenCreatingValidEvento_eventEventoCriadoIsRaised(self):
        bus = self.bus

        cmd = criar_evento_cmd(uuid4(), datetime.now(), 1)
        bus.send(cmd.build())
        self.assertIsNotNone(bus.messages[-1].criado_por)
        self.assertIsNotNone(bus.messages[-1].inicio)
        self.assertIsNotNone(bus.messages[-1].fim)
        bus.assert_received_type(EventoCriado)
        bus.assert_nothing_else()

class TestCamposEvento(TestCase):
    def setUp(self):
        self._tmp_dir = tempfile.mkdtemp()
        bus = TestBus()
        store = FileEventStore(bus, self._tmp_dir)
        EventoCommandHandlers.register(bus, store)

        # definir um novo evento ja criado
        cmd = criar_evento_cmd(uuid4(), datetime.now(), 1)
        bus.send(cmd.build())
        bus.assert_received_type(EventoCriado)
        bus.assert_nothing_else()

        self.store = store
        self.bus = bus
        self.evento_id = cmd.aggregate_id

    def tearDown(self):
        shutil.rmtree(self._tmp_dir)

    def test_definirCamposValidos(self):
        bus = self.bus
        evento_id = self.evento_id

        cmd = MessageBuilder() \
            .type(DefinirParametroEvento) \
            .aggregate(evento_id, 0) \
            .args('titulo', 'Evento de teste') \
            .build()

        bus.send(cmd)
        self.assertIsNotNone(bus.messages[-1].nome)
        self.assertIsNotNone(bus.messages[-1].valor)
        bus.assert_received_type(ParametroEventoDefinido)
        bus.assert_nothing_else()

    def test_definirCamposInvalidos(self):
        bus = self.bus
        evento_id = self.evento_id

        cmd = MessageBuilder() \
            .type(DefinirParametroEvento) \
            .aggregate(evento_id, 0) \
            .args('invalido', 'Evento de teste') \
            .build()

        bus.send(cmd)
        bus.assert_nothing_else()

    def test_publicarEventoComCamposEmFalta(self):
        bus = self.bus
        evento_id = self.evento_id

        cmd = MessageBuilder() \
            .type(PublicarEvento) \
            .aggregate(evento_id, 0) \
            .build()

        bus.send(cmd)
        bus.assert_nothing_else()

    def test_publicarEventoComCamposTodos(self):
        bus = self.bus
        evento_id = self.evento_id

        titulo = MessageBuilder() \
            .type(DefinirParametroEvento) \
            .aggregate(evento_id, 0) \
            .args('titulo', '') \
            .build()
        descricao = MessageBuilder() \
            .type(DefinirParametroEvento) \
            .aggregate(evento_id, 1) \
            .args('descricao', '') \
            .build()
        banner = MessageBuilder() \
            .type(DefinirParametroEvento) \
            .aggregate(evento_id, 2) \
            .args('banner', '') \
            .build()

        cmd = MessageBuilder() \
            .type(PublicarEvento) \
            .aggregate(evento_id, 3) \
            .build()

        bus.send(titulo)
        bus.send(descricao)
        bus.send(banner)
        bus.send(cmd)
        bus.assert_received_type(ParametroEventoDefinido)
        bus.assert_received_type(ParametroEventoDefinido)
        bus.assert_received_type(ParametroEventoDefinido)
        bus.assert_received_type(EventoPublicado)
        bus.assert_nothing_else()
