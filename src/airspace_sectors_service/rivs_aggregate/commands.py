from os import path
from dataclasses import dataclass
from functools import singledispatchmethod
from uuid import UUID

from src.kernel.repositories import Repository
from src.canonical_model.vatsim_ratings import PosicaoAtc

from .domain import Riv, CriarSector, AlterarFrequenciaSector

class RivCommandHandlers:
    @classmethod
    def register(cls, bus, store):
        obj = cls(store)
        bus.register(obj.handle, CriarSector, AlterarFrequenciaSector)
        return obj

    def __init__(self, store):
        self.repo = Repository(Riv, store)

    @singledispatchmethod
    def handle(self, cmd): pass

    @handle.register
    def _(self, cmd: CriarSector):
        riv = self.repo.get(cmd.aggregate_id)
        riv.handle(cmd)
        self.repo.save(riv, cmd.aggregate_version)

    @handle.register
    def _(self, cmd: AlterarFrequenciaSector):
        riv = self.repo.get(cmd.aggregate_id)
        riv.handle(cmd)
        self.repo.save(riv, cmd.aggregate_version)
