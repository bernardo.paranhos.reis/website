from datetime import datetime
import json
from flask import current_app as app
from flask_pymongo import PyMongo
from server.entities.atcratings import ATCRating

_db = PyMongo(app).db.atcmentors

def find_one_by_predicate(**predicate):
    return _db.find_one(predicate)

def find_mentor_by_trainee(cid):
    return [y for y in get_all() if cid in [x['cid'] for x in y['trainees']]]

def is_mentor(cid):
    return find_one_by_predicate(cid=cid) != None

def get_facility(cid):
    return ATCRating.get_facility(cid)

def get_avail_facilities(limit):
    return ATCRating.get_avail_facilities(limit)

def get_all():
    entities = list(_db.find())
    for entity in entities:
        entity['limit'] = ATCRating.get_facility(entity['limit'])
    return entities
