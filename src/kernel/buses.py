from collections import defaultdict

class MemoryBus:
    def __init__(self):
        self._handlers = defaultdict(list)

    def register(self, handler, *messages):
        for message in messages:
            self._handlers[message].append(handler)

    def send(self, message):
        handlers = self._get_handlers(message)
        if len(handlers) == 0:
            raise RuntimeError('No handler.', type(message))
        elif len(handlers) > 1:
            raise RuntimeError('More than one handler.', type(message))
        else:
            handlers[0](message)

    def publish(self, message):
        for handler in self._get_handlers(message):
            handler(message)

    def _get_handlers(self, message):
        return self._handlers[type(message)]

class TestBus(MemoryBus):
    def __init__(self):
        super().__init__()
        self.messages = []

    def publish(self, message):
        super().publish(message)
        self.messages.append(message)

    def assert_received_type(self, msg_type):
        for message in self.messages:
            if type(message) == msg_type:
                self.messages.remove(message)
                return
        raise AssertionError()

    def assert_nothing_else(self):
        if len(self.messages) != 0:
            raise AssertionError()
