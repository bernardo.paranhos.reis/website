import re
import requests
from dataclasses import dataclass
from dateutil import parser
from datetime import datetime
from functools import singledispatchmethod

from src.airspace_sectors_service import SectorCriado
from src.eventos_service import EventoCriado, EventoPublicado, ParametroEventoDefinido

TS_VIEWER_URL = r'https://www.tsviewer.com/ts3viewer.php?ID=1053934&text=757575&text_size=12&text_family=1&text_s_color=000000&text_s_weight=normal&text_s_style=normal&text_s_variant=normal&text_s_decoration=none&text_i_color=&text_i_weight=normal&text_i_style=normal&text_i_variant=normal&text_i_decoration=none&text_c_color=&text_c_weight=normal&text_c_style=normal&text_c_variant=normal&text_c_decoration=none&text_u_color=000000&text_u_weight=normal&text_u_style=normal&text_u_variant=normal&text_u_decoration=none&text_s_color_h=&text_s_weight_h=bold&text_s_style_h=normal&text_s_variant_h=normal&text_s_decoration_h=none&text_i_color_h=000000&text_i_weight_h=bold&text_i_style_h=normal&text_i_variant_h=normal&text_i_decoration_h=none&text_c_color_h=&text_c_weight_h=normal&text_c_style_h=normal&text_c_variant_h=normal&text_c_decoration_h=none&text_u_color_h=&text_u_weight_h=bold&text_u_style_h=normal&text_u_variant_h=normal&text_u_decoration_h=none&iconset=default'
VATSIM_STATUS_URL = r'http://cluster.data.vatsim.net/vatsim-data.json'
CALLSIGN_PATTERNS = re.compile(r'^LP[A-Z]{2}_([A-Z]_)?[A-Z]{3}$')

class HomePageModel:
    @classmethod
    def register(self, bus, db):
        home_page = HomePage(db)
        bus.register(home_page.handle, SectorCriado, EventoCriado, EventoPublicado, ParametroEventoDefinido)

class HomePage:
    def __init__(self, db):
        db.lista_sectores.drop()
        db.lista_eventos.drop()
        self.db = db

    @singledispatchmethod
    def handle(self, evt): pass

    @handle.register
    def _(self, evt: SectorCriado):
        lookup = {'callsign': evt.callsign}
        fields = {'callsign': evt.callsign, 'nome': evt.nome}
        self._update(self.db.lista_sectores, lookup, **fields)

    @handle.register
    def _(self, evt: EventoCriado):
        lookup = {'id': evt.aggregate_id}
        fields = {'ends': evt.fim, 'publicado': False}
        self._update(self.db.lista_eventos, lookup, **fields)

    @handle.register
    def _(self, evt: ParametroEventoDefinido):
        lookup = {'id': evt.aggregate_id}
        fields = {evt.nome: evt.valor}
        self._update(self.db.lista_eventos, lookup, **fields)

    @handle.register
    def _(self, evt: EventoPublicado):
        lookup = {'id': evt.aggregate_id}
        fields = {'publicado': True}
        self._update(self.db.lista_eventos, lookup, **fields)

    def _update(self, db, lookup, **fields):
        db.update_one(
            lookup,
            {'$set': {**fields} },
            upsert=True,
        )

class HomePageFacade:
    def __init__(self, db):
        self.db = db.home_page

    def proximo_evento(self):
        query = self.db.lista_eventos.aggregate([
            {
                '$match':{
                    'ends':{'$gte':datetime.utcnow()},
                    'publicado': True,
                }
            },
            {
                '$sort': {'ends': 1}
            },
            {
                '$limit': 1
            }
        ])
        for obj in query:
            return obj

    def get_home_page_view(self):
        stations = self._get_online_stations()
        return {
            'ts_viewer_url': TS_VIEWER_URL,
            'online_stations': stations,
            'nobody': len(stations) == 0,
            'proximo_evento': self.proximo_evento()
        }

    def _get_online_stations(self):
        def station_dto(c):
            # parse time client connected
            time_online = parser.parse(c['time_logon'])
            time_online = time_online.replace(tzinfo=None)
            # try get station name, use callsign otherwise
            station = self.db.lista_sectores.find_one({'callsign': c['callsign']})
            station_name = station['nome'] if station else c['callsign']

            return {
                'controller_name': c['realname'],
                'station_name'   : station_name,
                'frequencia'     : c['frequency'],
                'time_online'    : time_online,
            }

        clients = requests.get(VATSIM_STATUS_URL).json().get('clients', [])
        # filter ATCs
        clients = filter(lambda c: c['clienttype'] == 'ATC', clients)
        # filter interested callsigns
        pattern = CALLSIGN_PATTERNS
        lp_stations = lambda c: pattern.match(c['callsign']) is not None
        clients = filter(lp_stations, clients)
        # map to dtos
        return list(map(station_dto, clients))
